#include <iostream>
#include <fstream> 
#include <fcntl.h>
#include <cstring>
#include <unistd.h>
#include <cstdlib>
#include <sstream>
#include "muon.h"
using namespace std;


unsigned decode(unsigned s);



int main(int argc, char** argv)
{
	ifstream raw;
	MuonEvent mu;
	int datas[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	int n=0;
	int n4=0;
	double p;
	string pname, file;
	unsigned t_start, t_stop;
	
	if (argc < 2)
	{
		cout << "Usage: " << argv[0] << " pathname" << endl;
		exit(0);
	}
	else
	{
		pname = argv[1];
		pname += "/log-";
	}

	
	int n_file=1;
	file=pname+to_string(n_file);

	//Finche esistono i file log-i, li apre e li legge
	while ( access(file.c_str(), F_OK)!=-1 ) 
	{
		raw.open(file.c_str(), ios::binary);

		while (! raw.eof() )
		{ 
			raw.read((char*)&mu, sizeof(mu));
			if (!raw.eof())
			{
				mu.signal = decode(mu.signal);	
				
				if ( n_file == 1 && n == 0 ) 
				{
					t_start = mu.timestamp;
				}
				//Conta il numero di piani attivati e coincidenze a 4
				int t = 0;
				for (int i=0; i<8; i++)
				{
					unsigned char a = (mu.signal >> (7-i)) & 1;
					datas[i] += (int)a;
					if ( i%2==0 && a) t++;
				}
				if (t == 4) n4++;
				
				n++;
			}
		}
		raw.close();
		
		p = 1;
		t_stop = mu.timestamp;
		//Output dati cumulativi dei file
		cout << n << " eventi" << endl;
		cout << "rate: " << (double)n/(t_stop-t_start) << "Hz\n";
		for (int i=0; i<4; i++)
		{
			cout << "Comp basso del piano "<<i+1<<": "<< datas[2*i]  << endl;
			cout << "Comparatore alto scattato " << datas[2*i+1] << endl;
			cout << "Efficienza " << (double)datas[2*i]/n << endl;
			p*=(double)datas[2*i]/n;
		}
		cout << "Coincidenze 4: " << n4 << endl;
		cout << "Probab. Coincidenza 4: " << p << endl;
		cout << "Probab. Coincidenza 3: " << p*((n * (1./datas[0]+1./datas[2]+1./datas[4]+1./datas[6]))-4) << endl;
		cout << endl;

		n_file++;
		file = pname+to_string(n_file);			
	}
	
	return 0;
}





/* 
	Il byte fornito dall'FPGA 
	viene decodificato in un byte
	L1 H1 L2 H2 L3 H3 L4 H4
	in cui Li e' un evento con
	l'i-esima soglia bassa attiva
	e Hi i-esima soglia alta attiva
*/
unsigned decode(unsigned s)
{
	unsigned res;
	
	switch ((s >> 4) & 0xf)
	{
		case 0xf:
			res = 0xaa;
			break;
		case 0x1:
			res = 0x2a;
			break;
		case 0x2:
			res = 0x8a;
			break;
		case 0x4:
			res = 0xa2;
			break;
		case 0x8:
			res = 0xa8;
			break;
		default:
			res = 0;
			break;
	}

	//Soglie alte
	res |= ((~s >> 1)&1);
	res |= ((~s >> 2)&1) << 2;
	res |= ((~s >> 3)&1) << 4;
	res |= ((~s >> 3)&1) << 6;
	
		

	return res;
	printf ("%x %x\n", s, res);
}
