/*
*	Convertitore da formato raw di AMiCO a ROOT Tree
*	Compilazione:	$ g++ -o amico2root amico2root.cpp -I`root-config --incdir` `root-config --libs`
*	Utilizzo:		$ ./amico2root path/containing/datas/
*	Branches del Tree:
*		pil -> piano i-esimo, soglia rivelazione bassa		| short
*		pih -> piano i-esimo, soglia rivelazione alta		| short
*		timestamp -> timestamp del momento di rivelazione	| unsigned
*/


#include <iostream>
#include <fstream>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>
#include <cstdlib>
#include <sstream>
#include <iomanip>
#include "muon.h"
#include "TFile.h"
#include "TTree.h"
using namespace std;


unsigned decode(unsigned s);


int main(int argc, char** argv)
{
	ifstream raw;
	MuonEvent mu;
	short int datas[8];
	string pname, file;
	int n_file=1;

	if (argc < 2)
	{
		cout << "Usage: " << argv[0] << " pathname" << endl;
		exit(0);
	}
	else
	{
		pname = argv[1];
	}

	string time_string = pname.substr(pname.rfind('-')+1, 10);
	file = pname+"../root/amico-"+time_string+
	".root";
	pname += "/log-";

	//Tree setup
	TFile* out = TFile::Open(file.c_str(), "RECREATE");
	TTree tree("Events", "List of Events");

	tree.Branch("timestamp", &mu.timestamp);
	tree.Branch("p1l", &datas[0]);
	tree.Branch("p1h", &datas[1]);
	tree.Branch("p2l", &datas[2]);
	tree.Branch("p2h", &datas[3]);
	tree.Branch("p3l", &datas[4]);
	tree.Branch("p3h", &datas[5]);
	tree.Branch("p4l", &datas[6]);
	tree.Branch("p4h", &datas[7]);

	//Finche esistono i file log-i, li apre e li legge
	file=pname+to_string(n_file);
	while ( access(file.c_str(), F_OK)!=-1 )
	{
		memset(datas, 0, sizeof(datas));

		raw.open(file.c_str(), ios::binary);

		while (! raw.eof() )
		{
			raw.read((char*)&mu, sizeof(mu));
			if (!raw.eof())
			{
				mu.signal = decode(mu.signal);

				//Conta il numero di piani attivati e coincidenze a 4
				for (int i=0; i<8; i++)
				{
					unsigned char a = (mu.signal >> (7-i)) & 1;
					datas[i] = (int)a;
				}
				tree.Fill();
			}
		}
		raw.close();
		n_file++;
		file=pname+to_string(n_file);
	}

	tree.Write();
	out->Close();



	return 0;
}




/*
	Il byte fornito dall'FPGA
	viene decodificato in un byte
	L1 H1 L2 H2 L3 H3 L4 H4
	in cui Li e' un evento con
	l'i-esima soglia bassa attiva
	e Hi i-esima soglia alta attiva
*/
unsigned decode(unsigned s)
{
	unsigned res;

	switch ((s >> 4) & 0xf)
	{
		case 0xf:
			res = 0xaa;
			break;
		case 0x1:
			res = 0x2a;
			break;
		case 0x2:
			res = 0x8a;
			break;
		case 0x4:
			res = 0xa2;
			break;
		case 0x8:
			res = 0xa8;
			break;
		default:
			res = 0;
			break;
	}

	//Soglie alte
	res |= ((~s >> 1)&1);
	res |= ((~s >> 2)&1) << 2;
	res |= ((~s >> 3)&1) << 4;
	res |= ((~s >> 3)&1) << 6;

	return res;
}
