#include <fstream>
#include <ctime>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include <wiringPi.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "muon.h"
using namespace std;


//Comunicazione Potenziometro
#define CLK	15
#define CS	16
#define	SDI	1
#define RST	0

//Scintillatori
#define RST_PIN	 9
#define LAM_PIN	 3
#define DS0_PIN 14
#define DS1_PIN  6
#define DS2_PIN 13
#define DS3_PIN 12
#define DS4_PIN  5
#define DS5_PIN  4
#define DS6_PIN	10


volatile sig_atomic_t stop = 0;

//Routine scrittura su file
void flush_buffer(const char* filename, MuonEvent** buffer_addr, int n);
//Routine per settare il potenziometro
unsigned getFromVolt(double v);
void setPotentiometer(unsigned value);
//Gestione del SIGINT
void turnOff(int) { stop = 1; }


int main (int argc, char** argv)
{

	MuonEvent* buffer;
	int count=0;
	int input_pins[7] = {DS0_PIN, DS1_PIN, DS2_PIN, DS3_PIN, DS4_PIN, DS5_PIN, DS6_PIN};
	double vsipm;
	string fname;
	time_t t_start;
	struct timespec tms;

	//Setup GPIO
	wiringPiSetup();
	
	pinMode(LAM_PIN, INPUT);
	pinMode(RST_PIN, OUTPUT);

	for (int i=0; i<7; i++)
	{
		pinMode(input_pins[i], INPUT);
	}

	digitalWrite(RST_PIN, LOW);

	//Gestione della tensione ai SiPM
	if (argc == 2)
	{
		vsipm = atof(argv[1]);
	}
	else
	{
		cout << "Inserisci il valore di tensione ai sipm ( 26.7 - 33.2 )V\n";
		cin >> vsipm;
	}

	if ( vsipm <= 26.7 ) //Se il valore e' inferiore al minimo viene impostato il minimo
	{
		setPotentiometer(0x00);
	}
	else if ( vsipm >= 33.2) //Se il valore e' superiore al massimo viene impostato il massimo
	{
		setPotentiometer(0xff);
	}
	else
	{
		setPotentiometer( getFromVolt(vsipm) );
	}


	//Gestione del SIGINT
	signal(SIGINT, turnOff);
	signal(SIGCHLD, SIG_IGN);


	buffer = new MuonEvent[BUFFER_SIZE];
	t_start = time(NULL);
	
	//Se non esiste la path dei dati la crea
	if ( access("dati/", F_OK) == -1 )
	{
		mkdir("dati/", 0777);
	}

	fname = "dati/dati-"+to_string(t_start);
	mkdir(fname.c_str(), 0777);
	fname += "/log-";
	cout << "Data acquisition started at " << t_start <<  endl;

	while (1)
	{
		//Polling LAM
		if ( digitalRead(LAM_PIN) )
		{
			//Lettura del segnale
			buffer[count].signal = 0;
			for (int i=0; i<7; i++)
			{	
				//"accende" il bit relativo al segnale
				buffer[count].signal |= (digitalRead(input_pins[i]) << (7-i));
			}
			
			//buffer[count].timestamp = time(NULL);
			timespec_get(&tms, TIME_UTC);
			buffer[count].timestamp = tms.tv_sec;
			buffer[count].micros = tms.tv_nsec/1000;
			buffer[count].n = count;
			count++;
			//Scrittura al riempimento del buffer
			if ( count == BUFFER_SIZE)
			{
				count = 0;
				flush_buffer(fname.c_str(), &buffer, BUFFER_SIZE);
			}
			//Reset
			digitalWrite(RST_PIN, HIGH);
			digitalWrite(RST_PIN, LOW);
			delayMicroseconds(1);
		}
		//In caso di SIGINT
		if (stop)
		{
			flush_buffer(fname.c_str(), &buffer, count);
			setPotentiometer(0x00);
			delete [] buffer;
			exit(0);
		}
		
	}

	delete [] buffer;
	return 0;
}



void flush_buffer(const char* filename, MuonEvent** buffer_addr, int n)
{
	static unsigned n_file = 0;
	string fname = filename;
	//Cambio di buffer
	MuonEvent* old_buffer = *buffer_addr;
	*buffer_addr = new MuonEvent[BUFFER_SIZE];
	
	n_file++;
	fname = fname + to_string(n_file);

	//Un nuovo thread si occupa della scrittura
	if (!fork() )
	{
		fstream of;
		of.open(fname.c_str(), ios::app | ios::binary);
		of.write((char*)old_buffer, sizeof(MuonEvent)*n);
		cout << n << " events written at " << time(NULL) << endl;
		of.close();
		delete [] old_buffer;
		_exit(0);
	}
}



unsigned getFromVolt(double v)
{
	unsigned d;
	double d_prev = 0.0256*(49250 - 1.253764*1000000/(v-1.253764));

	d = (unsigned)( (d_prev - (double)((int)d_prev)) > 0.5 ? d_prev+1 : d_prev );

	return d;
}



void setPotentiometer(unsigned value)
{
	//Segue il protocollo di comunicazione col potenziometro digitale
	pinMode(CLK, OUTPUT);
	pinMode(CS, OUTPUT);
	pinMode(SDI, OUTPUT);
	pinMode(RST, OUTPUT);

	digitalWrite(CLK, LOW);
	digitalWrite(CS, HIGH);
	digitalWrite(RST, HIGH);
	delay(1);
	digitalWrite(CS, LOW);
	
	for (int i=0; i< 10; i++)
	{		
		digitalWrite(SDI, (unsigned) 0x200 >> i & value );	
		digitalWrite(CLK, HIGH);
		delay(1);
		digitalWrite(CLK, LOW);
		delay(1);

	}

	digitalWrite(CS, HIGH);
}

