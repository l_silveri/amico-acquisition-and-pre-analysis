#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TApplication.h>
#include "muon.h"
#include <iostream>
#include <TGraph.h>
#include <TH1F.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <string>

//Numero eventi su cui fare i punti nel grafico
#define G_VALUE 50.e3
#define GEOMETRICAL_FACT 0.4355 //m^2 * srad

int main (int argc, char** argv)
{
  TApplication app("analisi_amico", &argc, argv);
  TChain* tree_chain = new TChain("Events");
  short int datas[8];
  unsigned timestamp;
  int rate_t_start;
  TGraph* rate_graph;
  TGraph* rate_c_graph;
  TGraph* effi_graph[4];
  TGraph* effi_global_graph;
  double effi[4], effi_global, effi_ev_pl[4], effi_ev_tot[4];
  double rate_123, n_123, effi_123;
  double rate, rate_corretto;
  TCanvas* effi_canv[5];
  TCanvas* rate_canv[2];

  {
    register int argc = app.Argc();
    for (register int i=1; i<argc; i++)
    {
        tree_chain->Add(app.Argv(i));
    }
  }
  //Setting up Branches
  tree_chain->SetBranchAddress("timestamp", &timestamp);
  tree_chain->SetBranchAddress("p1l", &datas[0]);
  tree_chain->SetBranchAddress("p1h", &datas[1]);
  tree_chain->SetBranchAddress("p2l", &datas[2]);
  tree_chain->SetBranchAddress("p2h", &datas[3]);
  tree_chain->SetBranchAddress("p3l", &datas[4]);
  tree_chain->SetBranchAddress("p3h", &datas[5]);
  tree_chain->SetBranchAddress("p4l", &datas[6]);
  tree_chain->SetBranchAddress("p4h", &datas[7]);

  tree_chain->GetEntry(0);

  //Inizializzazione Grafico Rate
  rate_t_start = timestamp;
  rate_graph = new TGraph();
  rate_graph->SetTitle("Rate vs time");
  rate_graph->SetMarkerStyle(20);
  rate_graph->GetXaxis()->SetTitle("Time");
  rate_graph->GetYaxis()->SetTitle("Rate (Hz/(m^2*sr))");
  rate_graph->GetXaxis()->SetTimeDisplay(1);
  rate_graph->GetXaxis()->SetTimeFormat("%H:%M %d/%m/%Y %F1970-01-01 00:00:00");
  rate_graph->GetXaxis()->SetNdivisions(-503);
  rate_graph->GetXaxis()->SetRangeUser(1540200000, 1540225000);

  //Inizializzazione Grafico Rate Corretto per efficienza
  rate_c_graph = new TGraph();
  rate_c_graph->SetTitle("Rate corretto vs time");
  rate_c_graph->SetMarkerStyle(20);
  rate_c_graph->GetXaxis()->SetTitle("Time");
  rate_c_graph->GetYaxis()->SetTitle("Flux (Hz/(m^2*sr))");
  rate_c_graph->GetXaxis()->SetTimeDisplay(1);
  rate_c_graph->GetXaxis()->SetTimeFormat("%H:%M %d/%m/%Y %F1970-01-01 00:00:00");
  rate_c_graph->GetXaxis()->SetNdivisions(-503);
  rate_c_graph->GetXaxis()->SetRangeUser(1540200000, 1540225000);
  n_123 = 0;

  //Inizializzazione Grafici Efficienza
  for (int i=0; i<4; i++)
  {
    effi_graph[i] = new TGraph();
    effi_graph[i]->SetTitle(("Efficiency vs time #"+std::to_string(i+1)).c_str());
    effi_graph[i]->SetMarkerStyle(20);
    effi_graph[i]->GetXaxis()->SetTitle("Time");
    effi_graph[i]->GetYaxis()->SetTitle("Efficiency");
    effi_graph[i]->GetXaxis()->SetTimeDisplay(1);
    effi_graph[i]->GetXaxis()->SetTimeFormat("%H:%M %d/%m/%Y %F1970-01-01 00:00:00");
    effi_graph[i]->GetXaxis()->SetNdivisions(-503);
    effi_graph[i]->GetXaxis()->SetRangeUser(1540200000, 1540225000);
    effi_ev_pl[i]  = 0;
    effi_ev_tot[i] = 0;
  }

  effi_global_graph = new TGraph();
  effi_global_graph->SetTitle("Efficiency vs time");
  effi_global_graph->SetMarkerStyle(20);
  effi_global_graph->GetXaxis()->SetTitle("Time");
  effi_global_graph->GetYaxis()->SetTitle("Efficiency");
  effi_global_graph->GetXaxis()->SetTimeDisplay(1);
  effi_global_graph->GetXaxis()->SetTimeFormat("%H:%M %d/%m/%Y %F1970-01-01 00:00:00");
  effi_global_graph->GetXaxis()->SetNdivisions(-503);
  effi_global_graph->GetXaxis()->SetRangeUser(1540200000, 1540225000);

  int i=0;
  do
  {

    for (int j=0; j<4; j++)
    {
        if ( datas[0]+datas[2]+datas[4]+datas[6]-datas[2*j] == 3)
        {
          effi_ev_pl[j]+=datas[2*j];
          effi_ev_tot[j]++;
        }
    }
    if (datas[0] && datas[2] && datas[4])
    {
      n_123++;
    }

    if ( (i+1)%(int)G_VALUE == 0)
    {
        for (int j=0; j<4; j++)
        {
          effi[j]=effi_ev_pl[j]/effi_ev_tot[j];
        }

        effi_global = effi[0]*effi[1]*(effi[2]+effi[3]) +
                      effi[2]*effi[3]*(effi[0]+effi[1]) -
                      3*effi[0]*effi[1]*effi[2]*effi[3];

        effi_123 = effi[0]*effi[1]*effi[2];

        rate_123 = n_123 / (timestamp-rate_t_start) / GEOMETRICAL_FACT;
        rate = G_VALUE/(double)(timestamp-rate_t_start);
        rate_corretto = rate_123 / effi_123;
        rate_graph->SetPoint((i+1)/G_VALUE-1, rate_t_start,rate_123);
        rate_c_graph->SetPoint((i+1)/G_VALUE-1, rate_t_start,rate_corretto);
        effi_global_graph->SetPoint((i+1)/G_VALUE-1, rate_t_start,effi_123);
        n_123 = 0;

        for (int j=0; j<4; j++)
        {
          effi_graph[j]->SetPoint((i+1)/G_VALUE-1, rate_t_start, effi[j]);
          effi[j] = 0;
          effi_ev_pl[j]=0;
          effi_ev_tot[j]=0;
        }

        rate_t_start = timestamp;
    }

  } while (tree_chain->GetEntry(++i));


  rate_canv[0] = new TCanvas("amico_rate");
  rate_canv[0]->cd();
  rate_graph->Draw("ALP");

  rate_canv[1] = new TCanvas("amico_rate_corretto");
  rate_canv[1]->cd();
  rate_c_graph->Draw("ALP");


  for (int j=0; j<4; j++)
  {
    effi_canv[j] = new TCanvas(("amico_effi_"+std::to_string(j+1)).c_str());
    effi_canv[j]->cd();
    effi_graph[j]->Draw("ALP");
  }

  effi_canv[4] = new TCanvas("amico_effi");
  effi_canv[4]->cd();
  effi_global_graph->Draw("ALP");

  app.Run();
  return 0;
}
