acq: clock daq daq1k process #run from a Raspberry
data: a2r analyze process #run from a standard PC

# Thanks to Jan Panteltje
clock: ./src/clock.c
	gcc ./src/clock.c -o freq_pi -O4 -Wall -std=gnu99 -lm

daq: ./src/daq.cpp ./src/daq50k.cpp
	g++ ./src/daq50k.cpp -o daq -lwiringPi -Wall -std=c++11

daq1k: ./src/daq.cpp ./src/daq1k.cpp
	g++ ./src/daq1k.cpp -o daq1k -lwiringPi -Wall -std=c++11

process: ./src/process.cpp
	g++ ./src/process.cpp -o process -Wall -std=c++11

a2r: ./src/amico2root.cpp
	g++ ./src/amico2root.cpp -o amico2root -Wall -std=c++11 -I`root-config --incdir` `root-config --libs`

analyze: ./src/analyze.cpp
	g++ ./src/analyze.cpp -o analyze -Wall -std=c++11 -I`root-config --incdir` `root-config --libs`

clean:
	rm process daq daq1k amico2root clock
